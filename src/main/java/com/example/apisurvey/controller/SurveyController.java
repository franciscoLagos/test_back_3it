package com.example.apisurvey.controller;

import com.example.apisurvey.repository.model.Survey;
import com.example.apisurvey.service.SurveyService;
import com.example.apisurvey.service.impl.SurveyServiceImpl;
import com.example.apisurvey.utils.exception.ResourceAlreadyExists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLDataException;
import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api")
public class SurveyController {

    Logger logger = LogManager.getLogger(SurveyController.class);


    @Autowired
    private SurveyService surveyService;

    @PostMapping(value = "/survey/new")
    public Survey save(@RequestBody Survey survey)
    {
        logger.info("Controller se crea survey");
        //Survey survey1 = surveyService.save(survey);
        Survey surveyExit = surveyService.findByEmail(survey.getEmail());
        if (surveyExit != null) {

            throw new ResourceAlreadyExists("Resource already exists in DB.");
        } else {

           return surveyService.save(survey);
        }



    }

    @DeleteMapping("/survey/{id}")
    public Survey delete(@RequestBody Survey survey) {
        return surveyService.delete(survey);
    }

    @GetMapping(path = "survey/{id}")
    public Survey findById(@PathVariable Long id){
        return surveyService.findById(id);
    }

    @GetMapping(path = "survey/")
    public List<Survey> findAll(){
        return surveyService.findAll();
    }
}
