package com.example.apisurvey.utils.exception;

public class CustomException extends RuntimeException {

    public CustomException(String message) { super(message); }

}