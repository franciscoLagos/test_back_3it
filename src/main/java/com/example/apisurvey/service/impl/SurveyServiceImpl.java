package com.example.apisurvey.service.impl;

import com.example.apisurvey.repository.SurveyDAO;
import com.example.apisurvey.repository.model.Survey;
import com.example.apisurvey.service.SurveyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SurveyServiceImpl implements SurveyService {

    Logger logger = LogManager.getLogger(SurveyServiceImpl.class);


    @Autowired
    private SurveyDAO surveyDAO;

    @Override
    public Survey findById(Long id) {
        return surveyDAO.find(id);
    }

    @Override
    public Survey findByEmail(String email) {
        return surveyDAO.findByEmail(email);
    }

    @Override
    public Survey save(Survey survey) {
        Survey survey1 = new Survey();
        logger.info("se creara la encuesta");
        if (survey == null) {

            logger.error("survey is null");
        } else {
            survey1.setEmail(survey.getEmail());

            survey1.setBebida(survey.getBebida());


        }

        return surveyDAO.save(survey1);
    }

    @Override
    public Survey delete(Survey survey) {
        Survey survey1 = new Survey();
        survey1.setId(survey.getId());
        return surveyDAO.delete(survey1);
    }

    @Override
    public List<Survey> findAll() {
        return surveyDAO.findAll();
    }
}
