package com.example.apisurvey.service;

import com.example.apisurvey.repository.model.Survey;

import java.util.List;

public interface SurveyService {

    Survey findById(Long id);

    Survey findByEmail(String correo);

    List<Survey> findAll();

    Survey save(Survey survey);


    Survey delete(Survey survey);
}
