package com.example.apisurvey.repository;

import com.example.apisurvey.repository.model.Survey;

import java.util.List;


public interface SurveyDAO {

    Survey find(Long id);

    Survey findByEmail(String correo);

    List<Survey> findAll();

    Survey save(Survey survey);


    Survey updateCustomer(Survey survey);


    Survey delete(Survey survey);
}
