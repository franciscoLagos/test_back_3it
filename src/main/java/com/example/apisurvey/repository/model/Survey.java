package com.example.apisurvey.repository.model;


//import javax.persistence.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;




@Entity
public class Survey implements Serializable{

    private static final long serialVersionUID = -1467549273928275422L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name="email", unique = true)
    private String email;

    @NotNull
    @Column(name="bebida")
    private String bebida;


    public Survey(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBebida() {
        return bebida;
    }

    public void setBebida(String bebida) {
        this.bebida = bebida;
    }
}






