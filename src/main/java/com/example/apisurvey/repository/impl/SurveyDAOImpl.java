package com.example.apisurvey.repository.impl;

import com.example.apisurvey.repository.SurveyDAO;
import com.example.apisurvey.repository.model.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;


@Repository
public class SurveyDAOImpl implements SurveyDAO {

    @Autowired
    EntityManager em;


    public Survey find(Long id){
        if (id == null)return null;
        return em.find(Survey.class,id);
    }

    /*
    @Override
    public List<Survey> list (){
        TypedQuery<Survey> query = em.createNamedQuery("Survey.findAll", Survey.class);
        return query.getResultList().size()>0? query.getResultList(): Collections.emptyList() ;
    }*/


    @Override
    public Survey findByEmail(String email){
        TypedQuery<Survey> query = em.createQuery("select c from Survey c where c.email = ?1",Survey.class);

        query.setParameter(1, email);
        List<Survey> results = query.getResultList();
        if(!results.isEmpty()){
            return results.get(0);
        }
        else{
            return null;
        }

    }

    @Override
    public List<Survey> findAll(){
        TypedQuery<Survey> query = em.createQuery("select c from Survey c",Survey.class);

        List<Survey> results = query.getResultList();
        if(!results.isEmpty()){
            return results;
        }
        else{
            return null;
        }

    }

    @Override
    @Transactional
    public Survey save(Survey survey){
        if (survey == null)return null;
        em.persist(survey);
        return survey;
    }

    @Override
    @Transactional
    public Survey updateCustomer(Survey survey) {
        em.merge(survey);
        return survey;
    }

    @Override
    @Transactional
    public Survey delete(Survey survey){
        if (survey == null)return null;
        em.remove(survey);
        return survey;
    }
}
