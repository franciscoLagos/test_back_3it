# README #

Este es el readme para el test tecnico de 3IT

### Descripcion

* Este es un proyecto, en el cual se construye una Api Rest para prueba tecnica de 3IT

### Instrucciones ###

* Tener previamente instalado jdk 8, Git y maven 

* Clonar el proyecto en su directorio preferido, con el siguiente comando: git clone https://franciscoLagos@bitbucket.org/franciscoLagos/test_back_3it.git
* Ingresar a la carpeta del proyecto
* Dar el comando mvn clean
* Ejecutar comando mvn install
* Finalmente ejecutar la app con el comando: ./mvnw spring-
* Estara listo el despliegue de la api en el puerto 8080
